<?php

include "mongokoneksi.php";

//======Join with match======

$pipeline = [
    [
        '$match' => [
            'nama_kapal' => 'Kapal A'
        ]
    ],
    [
	'$project' =>[
		'_id' => 0,
		//'nama_kapal' => 1
		
	]
    ], 
    [
        '$lookup' => [
            'from' => 'tblat2',
	    'localField'=>'nama_kapal',
	    'foreignField'=>'nama_kapal',
	    'as'=>'tblkapal'
        ]
    ],
    [
        '$limit' => 100
    ]
];

$options=[];

$cursor = $collection->aggregate($pipeline, $options);

foreach ($cursor as $document) {
    
  $jsun=json_encode($document['tblkapal']);
 
}

echo $jsun;


?>