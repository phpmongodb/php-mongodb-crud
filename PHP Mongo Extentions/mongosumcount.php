<?php

include "mongokoneksi.php";

$command = new MongoDB\Driver\Command([
  'aggregate' => 'tblat1',
  'pipeline' => [
      ['$match'=>['nama_kapal'=>'Kapal A']],
      ['$group' => ['_id' => '$kolomA', 'Jumlah Tahun' => ['$sum' => '$tahun']]],
      ['$sort'=>['count'=>-1]],
  ],
  'cursor' => new stdClass,
]);
$cursor = $manager->executeCommand('dblat1', $command);

foreach ($cursor as $doc) {
print_r($doc);
}

?>